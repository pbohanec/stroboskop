# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://pbohanec@bitbucket.org/pbohanec/stroboskop.git
```

Naloga 6.2.3:

https://bitbucket.org/pbohanec/stroboskop/commits/353b4fe32a8fa3224230d71d718d0a2c864eb6ed

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/pbohanec/stroboskop/commits/7cda9c0ebc78f14f4c5e423f6077ab5bc7d4f3b5

Naloga 6.3.2:
https://bitbucket.org/pbohanec/stroboskop/commits/98c170e96922f2384cfbd52e618e9785f0fc5534

Naloga 6.3.3:
https://bitbucket.org/pbohanec/stroboskop/commits/0cfa05e353d74f880a7b4bfca25acb97899ce066

Naloga 6.3.4:
https://bitbucket.org/pbohanec/stroboskop/commits/ea22d6642bfa212eea16dec276bb17dc87a04b2c

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/pbohanec/stroboskop/commits/7713c5c59b252874796f9f1ac61e08b7ea76c02a

Naloga 6.4.2:
https://bitbucket.org/pbohanec/stroboskop/commits/c7cde141a91b893473bb94bb8a36f5f067cfb3c9

Naloga 6.4.3:
https://bitbucket.org/pbohanec/stroboskop/commits/c549680e9d21123835f25bca6af664b9882af6ea

Naloga 6.4.4:
https://bitbucket.org/pbohanec/stroboskop/commits/110540a74064df34e524346e7fef48d48d673391